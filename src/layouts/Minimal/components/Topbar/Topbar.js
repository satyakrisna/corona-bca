import React from 'react';
import { Link as RouterLink} from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { AppBar, Toolbar} from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {
    boxShadow: 'none',
    paddingLeft: '3%',
    paddingRight: '3%'
  },
  flexGrow: {
    flexGrow: 1
  },
  register: {
    color: '#FFFFFF',
    paddingTop: '4px',
    paddingBottom: '4px',
    borderRadius: '25px',
    paddingLeft: '2%',
    paddingRight: '2%',
    border: '1px solid #FFFFFF',
    marginTop: '25px',
    transition: "all ease 0.8s",
    textTransform: 'none',
    '&:hover' : {
      boxShadow: "inset 250px 0 0 0 #6f7cc3",
      border: '1px solid #6f7cc3',
      color: '#FFFFFF'
    }
  }
}));

const Topbar = props => {

  const { className, ...rest } = props;

  const classes = useStyles();

  return (
    <AppBar
      {...rest}
      className={clsx(classes.root, className)}
      style={{background: 'transparent'}}
      position="absolute"
    >
      <Toolbar>
        <RouterLink to="/">
          <img
            alt="Logo"
            src="/images/icon flat4.png"
            height="55"
            style={{marginTop: '14px'}}
          />
        </RouterLink>

      </Toolbar>
    </AppBar>
  );
};

Topbar.propTypes = {
  className: PropTypes.string
};

export default Topbar;
