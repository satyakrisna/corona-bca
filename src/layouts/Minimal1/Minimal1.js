import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';

import { Topbar1 } from './components';

const useStyles = makeStyles(() => ({
  root: {
    // paddingTop: 64,
    height: '100%'
  },
  content: {
    height: '100%'
  }
}));

const Minimal1 = props => {
  const { children } = props;

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Topbar1 />
      <main className={classes.content}>{children}</main>
    </div>
  );
};

Minimal1.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string
};

export default Minimal1;
