import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/styles';
import { useMediaQuery, Hidden, Fab, Tooltip, Badge } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MeetingRoomIcon from '@material-ui/icons/MeetingRoom';

import { Sidebar, Footer } from './components';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    [theme.breakpoints.up('sm')]: {
    }
  },
  shiftContent: {
    paddingLeft: 240
  },
  content: {
    height: '100%'
  },
  row: {
    marginLeft: theme.spacing(3),
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(3),
    marginRight: theme.spacing(3)
  },
  spacer: {
    flexGrow: 1
  },
  top: {
    backgroundColor: "#0041C3",
    color: "#FFFFFF"
  },
  signOutButton: {
    marginLeft: theme.spacing(1),
    backgroundColor: "#0041C3",
    color: "#FFFFFF"
  }
}));

const Main = props => {
  const { children } = props;

  const classes = useStyles();
  const history = useHistory();
  const theme = useTheme();
  const isDesktop = useMediaQuery(theme.breakpoints.up('lg'), {
    defaultMatches: true
  });

  const [openSidebar, setOpenSidebar] = useState(false);

  const handleSidebarOpen = () => {
    setOpenSidebar(true);
  };

  const handleSidebarClose = () => {
    setOpenSidebar(false);
  };

  const shouldOpenSidebar = isDesktop ? true : openSidebar;

  const handleLogout = () => {
    localStorage.clear()
    history.push("/login")
  }

  return (
    <div
      className={clsx({
        [classes.root]: true,
        [classes.shiftContent]: isDesktop
      })}
    >
      <Sidebar
        onClose={handleSidebarClose}
        open={shouldOpenSidebar}
        variant={isDesktop ? 'persistent' : 'temporary'}
      />
      <main className={classes.content}>
      <div className={classes.row}>
          <Hidden lgUp>
            <Fab size="medium" onClick={handleSidebarOpen} className={classes.top} aria-label="open">
              <MenuIcon />
            </Fab>
          </Hidden>
          <span className={classes.spacer} />
          <Fab size="medium" className={classes.top} aria-label="open" >
          <Tooltip title="Notification" placement="bottom">
              <Badge
                badgeContent={0}
                color="secondary"
              >
                <NotificationsIcon />
              </Badge>
          </Tooltip>
            </Fab>
            <Fab size="medium" className={classes.signOutButton} aria-label="open" onClick={handleLogout}>
            <Tooltip title="Logout" placement="bottom">
              <MeetingRoomIcon />
          </Tooltip>
            </Fab>
        </div>
        {children}
        <Footer />
      </main>
    </div>
  );
};

Main.propTypes = {
  children: PropTypes.node
};

export default Main;
