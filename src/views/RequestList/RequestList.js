import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';

import { RequestTable } from './components';
import { useHistory } from 'react-router-dom';
import moment from 'moment';
import bca from 'services/bca'
import { Typography, Card, CardContent, Dialog, DialogContent, DialogContentText, Tooltip, IconButton } from '@material-ui/core';
import ReactExport from "react-export-excel";
import jsPDF from "jspdf";
import "jspdf-autotable";

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  content: {
    marginTop: theme.spacing(2)
  },
  flash: {
    height : 200
  },
  row: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: theme.spacing(2),
    zIndex: "100",
    backgroundImage: `url("/images/danger.png")`,
    backgroundSize: "cover",
    backgroundPosition: "center center",
    borderRadius: '5px',
    padding: '12px',
    width: 57
  },
  iconColor: {
    color: '#FFFFFF'
  },
  export: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
  },
  spacer: {
    flexGrow: 1
  },
  title: {
    color: '#1A2038',
    paddingLeft: '16px',
    marginTop: "auto",
  },
  icon:{
    marginLeft:5, 
    marginRight:10
  },
  package: {
    display: "flex",
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(-4.8),
    zIndex: "100"
  },
  exportIcons: {
    maxWidth: 30,
    margin: '5%'
  }
}));

const RequestList = () => {
  const classes = useStyles();
  const history = useHistory();

  const [requests, setRequests] = useState([]);
  const [exportData, setExportData] = useState([]);
  const [loading, setLoading]= useState(true);
  const [openFlash, setOpenFlash]= useState(false);
  const [message, setMessage]= useState('');
  const [imgAge, setImgAge]=useState(Date.now);
  var generated_date = moment(new Date()).format('DD/MM/YYYY');
  const [size, setSize]=useState(10);
  const [page, setPage]=useState(0);
  const [count, setCount]=useState(0);
  const [pages, setPages]=useState(0);
  const [openFlashErr, setOpenFlashErr]= useState(false);

  const ExcelFile = ReactExport.ExcelFile;
  const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
  const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

  const getData = () => {
    bca.get(`/bank-side/api/company?active=0&is_deleted=0`).then(res =>{
      const data = res.data.output_schema.company_requests
      setRequests(data.content)
      setCount(data.total_elements);
      setPages(data.total_pages);
      setLoading(false)
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })

     bca.get(`/bank-side/api/company?active=0&is_deleted=0&size=99999`).then(res =>{
      const data = res.data.output_schema.company_requests
      setExportData(data.content)
    }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
    })
  }

  const handleSuccess= (msg) =>{
    setMessage(msg);
    setOpenFlash(true);
    setTimeout(
      function() {
        setOpenFlash(false);
      },
      3000
    );
    setImgAge(Date.now());
  }
  
  const handleError= (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
      },
      3000
    );
    setImgAge(Date.now());
  }

  const handleError401 = (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
        localStorage.clear();
        history.push("/")
      },
      3000
    );
    setImgAge(Date.now());
  }

  useEffect(()=>{
    document.title = "Corona | Request List"
    getData()
    
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[size,page])

  const exportPDF = () => {
    const unit = "pt";
    const size = "A4";
    const orientation = "landscape";

    const doc = new jsPDF(orientation, unit, size);

    doc.setFontSize(15);

    var today = moment(new Date()).format('DD/MM/YYYY');
    const title = "Request List";
    const offset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(title) * doc.internal.getFontSize() / 2);
    const headers = [["Company Name", "Email", "Phone", "Address", "Request Date"]];
    const date = `Generated on ${today}`;

    const data = exportData.map(data=> [data.name, data.email, data.phone, data.location, moment(data.created_date).format('DD/MM/YYYY')]);

    let content = {
      startY: 70,
      head: headers,
      body: data
    };

    doc.text(title, offset, 50);
    doc.setFontSize(5);
    doc.text(date, 745, 20, {
        styles: { fontSize: 12 },
    });
    doc.autoTable(content);
    doc.save(`request_list_${generated_date}.pdf`)
  }

  return (
    <div className={classes.root}>
      <Card>
        <CardContent>
          <div className={classes.content}>
          <div className={classes.export}>
            
          <Typography variant="h3" style={{marginBottom:20}}>Request List</Typography>
    <span className={classes.spacer} />
      <Tooltip title="Export as PDF" placement="top">
        <IconButton onClick={exportPDF}>
          <img src="/images/pdf.png" alt="PDF" className={classes.exportIcons}/>
        </IconButton>
      </Tooltip>
      <ExcelFile filename={`request_list_${generated_date}`} element={
        <Tooltip title="Export as XLS" placement="top">
          <IconButton>
            <img src="/images/xls.png" alt="XLS" className={classes.exportIcons}/>
          </IconButton>
        </Tooltip>
        }>
        <ExcelSheet data={exportData} name="request_list">
                <ExcelColumn label="Company Name" value="name"/>
                <ExcelColumn label="Email" value="email"/>
                <ExcelColumn label="Phone" value="phone"/>
                <ExcelColumn label="Address" value="location"/>
                <ExcelColumn label="Request Date" value="created_date"/>
              </ExcelSheet>
      </ExcelFile>
    </div>
            <RequestTable requests={requests} handleSuccess={handleSuccess} handleError={handleError} handleError401={handleError401} getData={getData} loading={loading} onLoadingChange={setLoading} size={size} onSizeChange={setSize} page={page} onPageChange={setPage} count={count} pages={pages}/>
          </div>
          <Dialog
              open={openFlash}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogContent>
                <img src={`/images/success.gif?${imgAge}`} className={classes.flash} alt="Success"/>
                  <Typography variant="h2" style={{textAlign:'center'}}>
                    Success!
                  </Typography>
                <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
                  {message}
                </DialogContentText>
              </DialogContent>
            </Dialog>

            <Dialog
        open={openFlashErr}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        >
        <DialogContent>
            <img src={`/images/err.jpg?${imgAge}`} className={classes.flash} alt="Error"/>
            <Typography variant="h2" style={{textAlign:'center'}}>
                Error!
            </Typography>
            
            <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
            {message}
            </DialogContentText>
        </DialogContent>
        </Dialog>
        </CardContent>
      </Card>
      
    </div>
  );
};

export default RequestList;
