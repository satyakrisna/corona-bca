import React from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';
import {
  Grid,
  Button,
  Typography
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundImage: 'url(/images/bg-new.png)',
    backgroundSize: 'auto 100%',
    backgroundPosition: 'right',
    backgroundRepeat: 'no-repeat',
    height: '100%',
    [theme.breakpoints.down('md')]: {
      background: 'linear-gradient(90deg, rgba(170,190,223  ,1) 0%, rgba(161,169,216,1) 73%)',
    }
  },
  grid: {
    height: '100%',
    width: "fit-content"
  },
  texth1: {
    color: '#272a3d',
    marginLeft: '13%',
    marginTop: '57%',
    fontWeight: '900',
    fontSize: '30pt',
    lineHeight: '1.5',
    [theme.breakpoints.down('md')]: {
      textAlign: 'center',
      marginLeft: '0',
      marginTop: '40%'
    }
  },
  buttonJoinNow: {
    marginLeft: '13%',
    marginTop: '15%',
    paddingLeft: '10%',
    paddingRight: '10%',
    background: 'linear-gradient(90deg, rgba(170,190,223  ,1) 0%, rgba(161,169,216,1) 80%)',
    color: '#FFFFFF',
    borderRadius: '25px',
    transition: "all ease 0.8s",
    '&:hover' : {
      boxShadow: "inset 250px 0 0 0 #6f7cc3",
    },
    [theme.breakpoints.down('md')]: {
      color: '#272a3d',
      background: 'transparent',
      marginLeft: '0',
      marginTop: '20%',
      border: '1px solid #272a3d',
      left: '50%',
      transform: "translate(-50%, -50%)",
      '&:hover' : {
        boxShadow: 'none',
        color: '#FFFFFF',
        backgroundColor: '#6f7cc3',
        textAlign: 'center',
        border: '1px solid #6f7cc3'
      },
    }
  },
}));

const Home = props => {
  const { history } = props;

  const classes = useStyles();

  const redirectRegister = event => {
    event.preventDefault();
    history.push('/register');
  };

  return (
    <div className={classes.root}>
      <Grid
        className={classes.grid}
        container
      >
        <Grid
          className={classes.quoteContainer}
          item
          lg={5}
        >
          <Typography
            variant="h1"
            className={classes.texth1}
          >
            Manage Company Expenses to Optimize Your Company Growth
          </Typography>

          <Button
            className={classes.buttonJoinNow}
            onClick={redirectRegister}
          >
            APPLY NOW  <ArrowRightAltIcon style={{paddingLeft: '7%'}} />
          </Button>

        </Grid>
      </Grid>
    </div>
  );
};

Home.propTypes = {
  history: PropTypes.object
};

export default withRouter(Home);
