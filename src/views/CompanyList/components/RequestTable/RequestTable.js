import React, { useState, Fragment } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { makeStyles, withStyles } from '@material-ui/styles';
import bca from 'services/bca'
import {
  Card,
  CardContent,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
  Dialog,
  DialogTitle,
  IconButton,
  DialogContent,
  DialogActions,
  Select,
  MenuItem,
  LinearProgress
} from '@material-ui/core';

import Skeleton from 'react-loading-skeleton';

import CloseIcon from '@material-ui/icons/Close';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';

const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 10,
    borderRadius: 5,
  },
  colorPrimary: {
    backgroundColor: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
  },
  bar: {
    borderRadius: 5,
    backgroundColor: '#1a90ff',
  },
}))(LinearProgress);

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 1050
  },
  nameContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  actions: {
    justifyContent: 'flex-end'
  },
  table: {
    width: "25%",
    backgroundColor: '#FAFAFA'
  },
  danger: {
    color: '#E53935'
  },
  dialog: {
    width: "60%",
    float: 'left'
  },
  row: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(1)
  },
  spacer: {
    flexGrow: 1
  },
  pagination:{
    marginLeft: theme.spacing(3)
  }
}));

const RequestTable = props => {
  const {requests, loading, handleError, handleError401, size, page, count, pages } = props;

  const classes = useStyles();

  const [request, setRequest] = useState ({});
  const [openDetail, setOpenDetail] = useState(false);

  const handleBackButtonClick = () => {
    if(page>0){
      props.onPageChange(page-1);
      props.onLoadingChange(true);
    }else{
      alert("this is min page")
    }
  };

  const handleNextButtonClick = () => {
    if(page+1<pages){
      props.onPageChange(page+1);
      props.onLoadingChange(true);
    }else{
      alert("this is max page")
    }
  };

  const handleRowsPerPageChange = event => {
    props.onSizeChange(event.target.value);
    props.onPageChange(0);
    props.onLoadingChange(true);
  };

  const handleClickOpenDetail = (id) => {
    bca.get(`/bank-side/api/company/${id}`).then(res =>{
      const data = res.data.output_schema.company_request
      setRequest(data)
        setOpenDetail(true);
    }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
    })
  }

  const handleCloseDetail = () => {
    setOpenDetail(false);
  }

  return (
    <Fragment>
        <PerfectScrollbar>
          <div className={classes.inner}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Company Name</TableCell>
                  <TableCell>Email</TableCell>
                  <TableCell>Location</TableCell>
                  <TableCell>Phone</TableCell>
                  <TableCell>Registration date</TableCell>
                </TableRow>
              </TableHead>
              {loading ?
              <TableBody>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
              </TableBody>
              :
              <TableBody>
                {requests.length === 0 ?
                    <TableRow>
                      <TableCell colSpan='5' style={{textAlign: 'center'}}> No data available </TableCell>
                    </TableRow>
                  :
                requests.slice(0, size).map(request => (
                  <TableRow
                    className={classes.tableRow}
                    hover
                    key={request.uuid}
                    onClick={() => { handleClickOpenDetail(request.uuid)}}
                  >
                    <TableCell>{request.name}</TableCell>
                    <TableCell>{request.email}</TableCell>
                    <TableCell>
                      {request.location}
                    </TableCell>
                    <TableCell>{request.phone}</TableCell>
                    <TableCell>
                      {moment(request.created_date).format('DD/MM/YYYY')}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
              }
            </Table>
          </div>
        </PerfectScrollbar>
        <div className={classes.row}>
          <Typography className={classes.pagination}>Total data : {count}</Typography>
          <span className={classes.spacer} />
          <Typography className={classes.pagination}>Rows per page : </Typography>
          <Select
            name="size"
            value={size}
            onChange={handleRowsPerPageChange}
            className={classes.pagination}
          >
            <MenuItem value={"5"}>5</MenuItem>
            <MenuItem value={"10"}>10</MenuItem>
            <MenuItem value={"25"}>25</MenuItem>
          </Select>
          <Typography className={classes.pagination}>Page {page+1} of {pages}</Typography>
          <IconButton onClick={handleBackButtonClick} disabled={page>0?false:true}>
             <KeyboardArrowLeftIcon />
          </IconButton>
          <IconButton onClick={handleNextButtonClick} disabled={page+1<pages?false:true}> 
            <KeyboardArrowRightIcon />
          </IconButton>
        </div>
        <Dialog
        open={openDetail}
        onClose={handleCloseDetail}
        fullWidth
        maxWidth="md"
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
        <div className={classes.row}>
            <Typography variant="h4" >
             Detail Company
            </Typography>
            <span className={classes.spacer} />
            <IconButton onClick={handleCloseDetail}>
              <CloseIcon />
            </IconButton>
          </div>
        </DialogTitle>
        <DialogContent>
          <Card>
          <CardContent>
                
            <center>
              <img src={request.logo} alt="logo" style={{height:100}}/>
            </center>
              <Table>
              <TableHead>
                <TableRow>
                  <TableCell align="center" colSpan={2}>{request.name}</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell className={classes.table}>Email</TableCell>
                  <TableCell>{request.email}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell className={classes.table}>Phone</TableCell>
                  <TableCell>{request.phone}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell className={classes.table}>Address</TableCell>
                  <TableCell>{request.location}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell className={classes.table}>Account Number</TableCell>
                  <TableCell>{request.account_number}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell className={classes.table}>Date</TableCell>
                  <TableCell>
                      {moment(request.created_date).format('DD/MM/YYYY')}
                    </TableCell>
                </TableRow>
              </TableBody>
            </Table>
            <br/>
            <Typography variant="h5" style={{textAlign: 'center'}}><b>Balance</b></Typography>
            <br/>
            <Typography variant="body1" style={{textAlign: 'center'}}>{request.balance_used}/{request.balance}</Typography>
            <br/>
            <BorderLinearProgress variant="determinate" value={request.balance_used/request.balance*100}/>
            <br/>
            <Typography variant="h5" style={{textAlign: 'center'}}><b>Credit Limit</b></Typography>
            <br/>
            <Typography variant="body1" style={{textAlign: 'center'}}>{request.credit_limit_used}/{request.credit_limit}</Typography>
            <br/>
            <BorderLinearProgress variant="determinate" value={request.credit_limit_used/request.credit_limit*100}/>
            </CardContent>
        </Card>
        </DialogContent>
        <DialogActions>
        </DialogActions>
      </Dialog>
        </Fragment>
  );
};

RequestTable.propTypes = {
  className: PropTypes.string,
  requests: PropTypes.array.isRequired
};

export default RequestTable;
