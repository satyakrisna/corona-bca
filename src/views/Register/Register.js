import React from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';
import {
  Grid,
  Button,
  TextField,
  Typography,
  Card,
} from '@material-ui/core';


const useStyles = makeStyles(theme => ({
  root: {
    backgroundImage: 'url(/images/bg-polos-new.png)',
    backgroundSize: 'auto 100%',
    backgroundPosition: 'right',
    backgroundRepeat: 'no-repeat',
    height: '100%',
    paddingTop: '17%',
    [theme.breakpoints.down('md')]: {
      background: 'linear-gradient(90deg, rgba(170,190,223  ,1) 0%, rgba(161,169,216,1) 73%)',
      paddingLeft: '5%',
      paddingRight: '5%',
      paddingBottom: '20%',
      height: 'auto'
    },
    '& label.Mui-focused': {
      color: '#9FA7D8',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: '#a6a6a6',
      },
      '&:hover fieldset': {
        borderColor: '#B3B3B3',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#9FA7D8',
      },
    },
  },
  card: {
    padding: '2%',
    paddingTop: '3%',
    [theme.breakpoints.down('md')]: {
      padding: '8% 5%',
    }
  },
  buttonRegister: {
    margin: '0.75%',
    marginTop: '2%',
    width: '98.5%',
    background: 'linear-gradient(90deg, rgba(170,190,223  ,1) 0%, rgba(161,169,216,1) 80%)',
    color: '#FFFFFF',
    // borderRadius: '25px',
    transition: "all ease 0.8s",
    '&:hover' : {
      boxShadow: "inset 1100px 0 0 0 #6f7cc3"
    },
  },
  grid: {
    paddingLeft: '0.8%',
    paddingRight: '0.8%',
    [theme.breakpoints.down('md')]: {
      padding: '0',
    }
  },
  textfieldMargin: {
    [theme.breakpoints.down('md')]: {
      marginBottom: '5%'
    }
  },
  titleRegister: {
    textAlign: 'center',
    marginBottom: '1%',
    fontWeight: '900',
    // color: '#9FA7D8',
    color: '#a6a6a6',
    lineHeight: '1.5',
    [theme.breakpoints.down('md')]: {
      color: '#272a3d',
    }
  }
}));

const Register = props => {

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container>
        <Grid item
          lg={1}
        >
        </Grid>

        <Grid item
          lg={10}
        >
          <Typography
              className={classes.titleRegister}
              variant="h2"
            >
              REGISTER YOUR COMPANY TO CORONA
            </Typography>

          <Card
            className={classes.card}
          >
            
            <Grid container>
              <Grid
                className={classes.grid}
                item lg={3} md={12} sm={12} xs={12}>
                <TextField 
                  className={classes.textfieldMargin}
                  fullWidth
                  label="Company Name"
                  variant="outlined" />
              </Grid>

              <Grid
                className={classes.grid}
                item lg={3} md={12} sm={12} xs={12}>
                <TextField 
                  className={classes.textfieldMargin}
                  fullWidth
                  label="Phone Number"
                  variant="outlined" />
              </Grid>

              <Grid
                className={classes.grid}
                item lg={3} md={12} sm={12} xs={12}>
                <TextField 
                  className={classes.textfieldMargin}
                  fullWidth
                  label="Email"
                  variant="outlined" />
              </Grid>

              <Grid
                className={classes.grid}
                item lg={3} md={12} sm={12} xs={12}>
                <TextField 
                  className={classes.textfieldMargin}
                  fullWidth
                  label="Account Number"
                  variant="outlined" />
              </Grid>
            </Grid>

            <Button
              className={classes.buttonRegister}
            >
              REGISTER NOW <ArrowRightAltIcon style={{marginLeft: '1%'}} />
            </Button>

          </Card>
        </Grid>

      </Grid>
    </div>
  );
};

Register.propTypes = {
  history: PropTypes.object
};

export default withRouter(Register);
