import React, {useState, useEffect} from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid, Dialog, DialogContent, Typography, DialogContentText } from '@material-ui/core';
import { useHistory } from 'react-router-dom';

import {
  Budget,
  TotalUsers,
  LatestSales,
  TopGlobal
} from './components';
import bca from 'services/bca'

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  },flash: {
    height : 200
  },
}));

const Dashboard = () => {
  const classes = useStyles();
  const history = useHistory();

  const [data, setData] = useState({});
  const [dataReimburse, setDataReimburse] = useState({});
  const [dataVirtualCard, setDataVirtualCard] = useState({});
  const [message, setMessage]= useState('');
  const [openFlashErr, setOpenFlashErr]= useState(false);
  const [imgAge, setImgAge]=useState(Date.now);

  const handleError= (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
      },
      3000
    );
    setImgAge(Date.now());
  }

  const handleError401 = (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
        localStorage.clear();
        history.push("/")
      },
      3000
    );
    setImgAge(Date.now());
  }

  useEffect(() => {
    bca.get(`/api/getbcadashboard`)
    .then(res =>{
         setData(res.data);
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })

     bca.get(`/api/getcompanyreimbursemonthly?range=5`)
    .then(res =>{
      setDataReimburse(res.data.output_schema);
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })

     bca.get(`/api/getcompanyvirtualcardmonthly?range=5`)
    .then(res =>{
      setDataVirtualCard(res.data.output_schema);
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })
     // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className={classes.root}>
      <Grid
        container
        spacing={4}
      >
        <Grid
          item
          lg={6}
          sm={6}
          xl={6}
          xs={12}
        >
          <Budget data={data}/>
        </Grid>
        <Grid
          item
          lg={6}
          sm={6}
          xl={6}
          xs={12}
        >
          <TotalUsers data={data}/>
        </Grid>
        <Grid
          item
          lg={6}
          sm={6}
          xl={6}
          xs={12}
        >
          <LatestSales dataReimburse={dataReimburse}/>
        </Grid>
        <Grid
          item
          lg={6}
          sm={6}
          xl={6}
          xs={12}
        >
          <TopGlobal dataVirtualCard={dataVirtualCard}/>
        </Grid>
      </Grid>
      <Dialog
        open={openFlashErr}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        >
        <DialogContent>
            <img src={`/images/err.jpg?${imgAge}`} className={classes.flash} alt="Error"/>
            <Typography variant="h2" style={{textAlign:'center'}}>
                Error!
            </Typography>
            
            <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
            {message}
            </DialogContentText>
        </DialogContent>
        </Dialog>
    </div>
  );
};

export default Dashboard;
